{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}
unit FileOperation;

interface

uses
  Windows, Messages, SysUtils, Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, ShellApi, ShLwAPI;

type
	TShFlags = (shAllowUndo, shFilesOnly, shNoConfirmation, shNoConfirmMkDir,
			        shRenameOnCollision, shSilent, shSimpleProgress);
	TShFlag = Set of TShFlags;
	TShOp = (shCopy, shDelete, shMove, shRename);

	TFileOperation = class(TComponent)
	private
    { Private declarations }
	  FHParent: THandle;
	  FOperation: TShOp;
	  FTo: string;
	  FFlags: TShFlag;
	  FFromList: TStringList;
	  FAborted: Boolean;
	  FTitle: string;
	  FKeepFilesList: Boolean;
    FError: string;
	  procedure SetFilesList(aList: TStringList);
	protected
    { Protected declarations }
	public
    { Pulic declarations }
	  constructor Create(aOwner: TComponent); override;
	  destructor Destroy; override;
	  function Execute: boolean;
	  property Aborted: boolean read fAborted;
	published
    { Published declarations }
	  property Operation: TShOp read FOperation write FOperation default shCopy;
	  property FilesList: TStringList read FFromList write SetFilesList;
	  property Destination: string read FTo write FTo;
	  property Title: string read FTitle write FTitle;
	  property Options: TShFlag read FFlags write FFlags default [shAllowUndo];
	  property KeepFilesList: Boolean read FKeepFilesList write FKeepFilesList default false;
	end;

  function RelativeToAbsolutePath(const RelPath, BasePath: string): string;

implementation

function RelativeToAbsolutePath(const RelPath, BasePath: string): string;
var
  Dst: array[0..MAX_PATH-1] of char;
begin
  PathCanonicalize(@Dst[0], PChar(IncludeTrailingPathDelimiter(BasePath) + RelPath));
  result := Dst;
end;

constructor TFileOperation.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

	if (aOwner is TWinControl) then FHParent := TWinControl(aOwner).Handle
	else FHParent := 0;

	FOperation := shCopy;
	FTo := '';
  FFlags := [shAllowUndo];
  FFromList := TStringList.Create;
  FAborted := false;
  FTitle := '';
  FKeepFilesList := false;
end;

destructor TFileOperation.Destroy;
begin
  FFromList.Free;
	inherited Destroy;
end;

function TFileOperation.Execute: Boolean;
var
  TmpBuf: string;
	SHFOS: TSHFileOpStruct;
	i: Integer;
  fileOpResult: Integer;
begin
	Result := true;
	TmpBuf := '';

  {Get source files}
  if (FFromList.Count > 0) then
    {
    All filenames are put together in an unique buffer,
		separated by a ; character + one at the end
    }
	  for i := 0 to (FFromList.Count-1) do
      if (FFromList[i] <> '') then TmpBuf := TmpBuf + FFromList[i] + ';';
	  if (TmpBuf = '') then Exit;

    {
    Before using the buffer each ; must be replaced by a
		null character. Therefore the buffer will end with
		2 nulls (it's important)
    }
	  for i := 1 to Length(TmpBuf) do
      if (TmpBuf[i] = ';') then TmpBuf[i] := #0;

    {
    The Hwnd of the parent window is necessary, otherwise
		the task would be independant
    }
	  SHFOS.Wnd := FHParent;

    case fOperation of
	    shCopy: SHFOS.wFunc := FO_COPY;
	    shDelete: SHFOS.wFunc := FO_DELETE;
	    shMove: SHFOS.wFunc := FO_MOVE;
	    shRename: SHFOS.wFunc := FO_RENAME;
    end;

    SHFOS.pFrom := PChar(TmpBuf);
	  SHFOS.pTo := PChar(fTo);
    SHFOS.fFlags := 0;

    if (shAllowUndo in FFlags) then SHFOS.FFlags := SHFOS.fFlags or FOF_ALLOWUNDO;
    if (shFilesOnly in FFlags) then SHFOS.FFlags := SHFOS.fFlags or FOF_FILESONLY;
	  if (shNoConfirmation in FFlags) then SHFOS.FFlags := SHFOS.FFlags or FOF_NOCONFIRMATION;
    if (shNoConfirmMkDir in FFlags) then SHFOS.FFlags := SHFOS.FFlags or FOF_NOCONFIRMMKDIR;
    if (shRenameOnCollision in FFlags) then SHFOS.FFlags := SHFOS.FFlags or FOF_RENAMEONCOLLISION;
    if (shSilent in FFlags) then SHFOS.FFlags := SHFOS.FFlags or FOF_SILENT;
    if (shSimpleProgress in FFlags) then SHFOS.FFlags := SHFOS.FFlags or FOF_SIMPLEPROGRESS;

    SHFOS.fAnyOperationsAborted := false;
    SHFOS.hNameMappings := nil;
	  SHFOS.lpszProgressTitle := PChar(FTitle);

    fileOpResult := ShFileOperation(SHFOS);
    Result := (fileOpResult = 0);
    if not(Result) then
    begin
      FError := SysErrorMessage(fileOpResult);
    end;
	  FAborted := SHFOS.fAnyOperationsAborted;
    if not(FKeepFilesList) then FFromList.Clear;
end;

procedure TFileOperation.SetFilesList(aList: TStringList);
begin
	FFromList.Assign(aList);
end;

end.
